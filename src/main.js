import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vuetify from 'vuetify'
import axios from '@/plugins/axios'
import DateRangePicker from 'vue2-daterange-picker'
import 'vuetify/dist/vuetify.min.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '@/assets/css/main.css'
Vue.use(Vuetify)
Vue.use(DateRangePicker)
store.$axios = axios
Vue.prototype.$axios = axios

console.log(store)

Vue.config.productionTip = false

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app')
