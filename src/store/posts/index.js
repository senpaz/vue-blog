export default {
	state: {
		posts: [],
	},
	mutations: {
		creatPost(store, payload) {
			store.posts.push(payload)
		},
		loadPosts(store, payload) {
			store.posts = payload
		},
		loadPostsById(store, payload) {
			store.postsId = payload
		},
	},
	actions: {
		newPost({ commit }, form) {
			form.id = Math.round(Math.random() * 10000)
			commit('creatPost', form)
		},
		async fetchPost({ commit }, list) {
			try {
				const data = await this.$axios.get('/posts?page=1&per_page=1')
				commit('loadPosts', data && data.data)
				return data && data.data
			} catch (error) {
				console.log(error);
			}
		},
		async fetchPostById({ commit }, id) {
			try {
				const data = await this.$axios.get(`/posts/${id}`)
				commit('loadPostsById', data && data.data)
				console.log(data);
				return data && data.data && data.data.post
			} catch (error) {
				console.log(error);
			}
		}

	},
	getters: {
		posts(state) {
			return state.posts
		}
	}
}
