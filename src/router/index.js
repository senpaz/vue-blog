import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

const routes = [
	{
		path: '/',
		name: 'Home',
		component: () => import('../views/Home.vue')
	},
	{
		path: '/posts/:id',
		props: true,
		name: 'post',
		component: () => import('@/components/Posts/Post.vue')
	},
	{
		path: '/posts/none',
		redirect: {
			name: 'Home'
		}
	},
	{
		path: '/new',
		name: 'NewPost',
		component: () => import('../views/newPost.vue')
	},
	{
		path: '/posts',
		redirect: {
			name: 'Home'
		}
	},
	{
		path: '/utils',
		name: 'utils',
		component: () => import('../views/utils.vue')
	},
	{
		path: '*',
		component: () => import('../views/404')
	},
]

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes
})

export default router
